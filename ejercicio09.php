<!-- AND y OR -->

<?php

if ($_POST) {
    $valueA = $_POST['textValueA'];
    $valueB = $_POST['textValueB'];

    if (($valueA != $valueB) && ($valueA > $valueB)) {
        echo "El valor A ($valueA) es diferente y MAYOR que el valor B ($valueB)";
    }

}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 9</title>
</head>
<body>
    <form action="ejercicio09.php" method="POST">
        Value A:
        <input type="text" name="textValueA"><br>
        Value B:
        <input type="text" name="textValueB"><br>
        <input type="submit" value="Calc">
    </form>
</body>
</html>