<!-- OPERADORES LOGICOS -->

<?php

if ($_POST) {
    $valueA = $_POST['textValueA'];
    $valueB = $_POST['textValueB'];

    if ($valueA === $valueB) 
        echo "El valor A ($valueA) es igual que el valor B ($valueB)";
    else if($valueA > $valueB)
        echo "El valor A ($valueA) es MAYOR que el valor B ($valueB)";
    else
        echo "El valor A ($valueA) es MENOR que el valor B ($valueB)";
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 8</title>
</head>
<body>
    <form action="ejercicio08.php" method="POST">
        Value A:
        <input type="text" name="textValueA"><br>
        Value B:
        <input type="text" name="textValueB"><br>
        <input type="submit" value="Calc">
    </form>
</body>
</html>