<!-- Conexion a base de datos (Mysql) -->

<?php

$server = "localhost";
$user = "root";
$password = "";

try{
    $conexion = new PDO("mysql:host=$server;dbname=album", $user, $password);
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Conexion establecida";

    // Insertar dato
    $sql = "INSERT INTO `fotos` (`id`, `nombre`, `ruta`) VALUES (NULL, 'prueba03', 'prueba03.png');";
    $conexion->exec($sql);

}
catch(PDOException $error){
    echo "Error de conexion: $error";
}

?>