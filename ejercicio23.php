<!-- Leer Arrays, Funciones de arrays -->

<?php

$frutas = array('manzana', 'fresa', 'uva');
array_push($frutas,'pera');
print_r($frutas);

echo '<br><br>';

// Un valor 
$frutas[4] = 'mango';
echo "frutas[2] = $frutas[2]<br><br>";

// Todo el contenido
foreach ($frutas as $key => $value) {
    echo "frutas [$key] = $value <br>";
}

// NOTA: paso de valor por referencia 
// foreach ($frutas as $key => &$value) {

?>