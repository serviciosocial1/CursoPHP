<!-- Consulta de datos -->

<?php

$server = "localhost";
$user = "root";
$password = "";

try{
    $conexion = new PDO("mysql:host=$server;dbname=album", $user, $password);
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Conexion establecida<br><br>";

    // Consultar datos de tabla
    $sqlConsulta = "SELECT * FROM `fotos`;";
    $sentecia = $conexion->prepare($sqlConsulta);
    $sentecia->execute();

    $resultado = $sentecia->fetchAll();

    print_r($resultado);
    foreach($resultado as $foto){
        echo '<br>'.$foto['nombre'];
    }

}
catch(PDOException $error){
    echo "Error de conexion: $error";
}

?>