<!-- Metodo constructor en PHP -->

<?php

class persona {

    // Propiedades
    public $nombre;
    private $edad;          
    protected $altura;

    // Constructor
    function __construct($nuevoNombre){
        $this->nombre = $nuevoNombre;
    }

    // Metodos
    public function asignarNombre($nuevoNombre){
        $this->nombre = $nuevoNombre;
    }
    public function imprimirNombre(){
        echo "Hola soy: $this->nombre <br>";
    } 
    public function mostrarEdad($edad){
        $this->edad = $edad;
        return $this->edad;
    }
    public function mostrarAltura($altura){
        $this->$altura = $altura;
        return $this->$altura;
    }
}

// Instancia
$objAlumno1 = new persona('Oscar');

// metodos
$objAlumno1->imprimirNombre();

?>